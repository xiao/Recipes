//
//  UIImageView+DownloadImage.h
//  Recipes
//
//  Created by jin on 10/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (DownloadImage)

- (NSURLSessionDownloadTask *)loadImage:(NSURL *)url;
@end
