
#import "UIImageView+DownloadImage.h"

@implementation UIImageView (DownloadImage)
- (NSURLSessionDownloadTask *)loadImage:(NSURL *)url {
    NSURLSession *session = [NSURLSession sharedSession];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDownloadTask *downloadtask = [session downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (!error) {
            NSData *data = [NSData dataWithContentsOfURL:location];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                strongSelf.image = image;
            });
        }
    }];
    [downloadtask resume];
    return downloadtask;
}
@end
