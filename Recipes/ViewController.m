#import <RXPromise/RXPromise.h>
#import "SVProgressHUD.h"
#import "ViewController.h"
#import "ReSimpleTableViewCell.h"
#import "RePopularTableViewCell.h"
#import "RecipesHeaderView.h"
#import "ReciptDetailViewModel.h"
#import "RecipeDetailsViewController.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource, RePopularTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableview;
@property (nonatomic, strong) UICollectionView *collection;
@property (nonatomic, strong) NSArray *otherResults;
@property (nonatomic, strong) NSArray *popularResults;

@property (nonatomic, assign) CGFloat popularSectionHeight;

@end

@implementation ViewController

- (void)loadView {
    [super loadView];
    self.popularSectionHeight = 240.0f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.otherResults = [NSArray new];
    self.popularResults = [NSArray new];
    [self setupTableView];
    self.title = @"My Recipes";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)loadData {
    [SVProgressHUD show];
    ReciptDetailViewModel *viewModel = [[ReciptDetailViewModel alloc] init];
    [viewModel download]
    .thenOnMain(^id(id result) {
        NSArray *data = (NSArray *)result;
        NSMutableArray *other = [NSMutableArray array];
        NSMutableArray *popular = [NSMutableArray array];
        for (Recipe *recipt in data) {
            if (recipt.thumbnail.length) {
                [popular addObject:recipt];
            }else {
                [other addObject:recipt];
            }
        }
        self.otherResults = other;
        self.popularResults = popular;
        [self.tableview reloadData];
        [SVProgressHUD dismiss];
        return nil;
    }, ^id(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Failed to get the data"];
        NSLog(@"%@",error.localizedDescription);
        return nil;
    });
    
}

- (void)setupTableView {
    self.tableview = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableview.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableview registerClass:[RePopularTableViewCell class] forCellReuseIdentifier:RePopularTableViewCellString];
    [self.tableview registerClass:[ReSimpleTableViewCell class] forCellReuseIdentifier:ReSimpleTableViewCellString];
    [self.tableview registerClass:[RecipesHeaderView class] forHeaderFooterViewReuseIdentifier:RecipesHeaderViewString];
    [self.view addSubview:self.tableview];
    NSDictionary *views = NSDictionaryOfVariableBindings(_tableview);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableview]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableview]|" options:0 metrics:nil views:views]];
}


- (void) willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.recipe.rotation" object:nil userInfo:@{@"trait":@([newCollection verticalSizeClass])}];
    
    if ([newCollection verticalSizeClass] == UIUserInterfaceSizeClassCompact) {
        self.popularSectionHeight = 340.0f;
    }else if ([newCollection verticalSizeClass] == UIUserInterfaceSizeClassRegular || [newCollection verticalSizeClass] == UIUserInterfaceSizeClassUnspecified) {
        self.popularSectionHeight = 240.0f;

    }
    [self.tableview reloadData];
}

#pragma mark -- UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else {
        return self.otherResults.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        RePopularTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RePopularTableViewCellString];
        cell.recipes = self.popularResults;
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else {
        ReSimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ReSimpleTableViewCellString];
        cell.recipe = [self.otherResults objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title = section == 0? @"Popular Recipes" : @"Other Recipes";
    RecipesHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:RecipesHeaderViewString];
    view.title = title;
    return view;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? self.popularSectionHeight: 65.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.section == 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        Recipe *recipe = [self.otherResults objectAtIndex:indexPath.row];
        RecipeDetailsViewController *detailViewController =  [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"RecipeDetailsViewController"];
        detailViewController.recipe = recipe;
        [self.navigationController pushViewController:detailViewController animated:YES];    }

}

#pragma mark -- RepopularTableViewCell Delegate

- (void)didSelectItemInPopularSection:(NSInteger)index {
    [self showRecipeDetailPage:index];
}


- (void)showRecipeDetailPage:(NSInteger)index {
    Recipe *recipe = [self.popularResults objectAtIndex:index];
    RecipeDetailsViewController *detailViewController =  [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"RecipeDetailsViewController"];
    detailViewController.recipe = recipe;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
