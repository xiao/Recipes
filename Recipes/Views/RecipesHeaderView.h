#import <UIKit/UIKit.h>
extern NSString *const RecipesHeaderViewString;

@interface RecipesHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) NSString *title;
@end
