#import "ReDetailMainTableViewCell.h"
#import "UIImageView+DownloadImage.h"

NSString *const ReDetailMainTableViewCellString = @"ReDetailMainTableViewCell";

@interface ReDetailMainTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *theImage;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *buttonLiked;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleViewLeading;
@property (weak, nonatomic) IBOutlet UIButton *link;

@end

@implementation ReDetailMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"com.recipe.rotation"  object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        int value = [[note userInfo][@"trait"] intValue];
        if (value == UIUserInterfaceSizeClassCompact) {
            self.imageTrailing.constant = 100;
            self.imageLeading.constant = 100;
            self.titleViewLeading.constant = 100;
            self.titleViewTrailing.constant = 100;
        }else if (value == UIUserInterfaceSizeClassRegular || value == UIUserInterfaceSizeClassUnspecified) {
            self.imageLeading.constant = 0;
            self.imageTrailing.constant = 0;
            self.titleViewLeading.constant = 0;
            self.titleViewTrailing.constant = 0;

        }
        [self setNeedsLayout];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setRecipe:(Recipe *)recipe {
    _recipe = recipe;
    [self updateContent];
}

- (void)updateContent {
    self.title.text = self.recipe.title;
    self.theImage.image = [UIImage imageNamed:@"default"];
    if (self.recipe.thumbnail.length) {
        NSURL *requesturl = [NSURL URLWithString:self.recipe.thumbnail];
        [self.theImage loadImage:requesturl];
    }
    [self.link setTitle:self.recipe.href forState:UIControlStateNormal];
}

- (IBAction)likeClicked:(id)sender {
    self.buttonLiked.selected = !self.buttonLiked.selected;
}


- (IBAction)openWebLink:(id)sender {
    if ([_delegate respondsToSelector:@selector(didOutsideWebLinkClicked:)]) {
        [_delegate didOutsideWebLinkClicked:[NSURL URLWithString:self.recipe.href]];
    }
}

@end
