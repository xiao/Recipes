//
//  HorizontalFlowLayout.m
//  Recipes
//
//  Created by jin on 9/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

#import "HorizontalFlowLayout.h"

@implementation HorizontalFlowLayout
- (instancetype) init {
    if (self = [super init]) {
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.sectionInset = UIEdgeInsetsZero;
        self.minimumLineSpacing = kLineSpacing;
        self.minimumInteritemSpacing = kItemSpacing;
    }
    return self;
}
@end
