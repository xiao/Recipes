#import "ReSimpleTableViewCell.h"
#import "Recipe.h"

NSString *const ReSimpleTableViewCellString = @"ReSimpleTableViewCell";

@interface ReSimpleTableViewCell()
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIView *bottomLine;
@end

@implementation ReSimpleTableViewCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupContentView];
    }
    return self;
}

- (void)setRecipe:(Recipe *)recipe {
    _recipe = recipe;
    [self.label setText:self.recipe.title];
}

- (void)setupContentView {
    self.label = [[UILabel alloc] initWithFrame:CGRectZero];
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    self.label.numberOfLines = 2;
    [self.label setFont:[UIFont fontWithName:@"Times New Roman" size:14.0f]];
    [self.label setTextColor:[UIColor colorWithRed:65.0f/255.0f green:65.0f/255.0f blue:65.0f/255.0f alpha:1.0]];
    [self.contentView addSubview:self.label];
    
    self.bottomLine = [UIView new];
    self.bottomLine.backgroundColor = [UIColor colorWithRed:220/255 green:220/255 blue:220/255 alpha:1.0];
    self.bottomLine.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.bottomLine];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSMutableArray *constrains = [NSMutableArray array];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:16.0f]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:16.0f]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1]];
    [NSLayoutConstraint activateConstraints:constrains];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
