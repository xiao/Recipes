#import <UIKit/UIKit.h>
#import "Recipe.h"

extern NSString *const RePopularCollectionViewCellString;

@interface RePopularCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) Recipe *recipe;
@property (nonatomic, strong) UIImageView *imageView;
@end
