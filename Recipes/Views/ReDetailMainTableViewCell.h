#import <UIKit/UIKit.h>
#import "Recipe.h"
extern NSString *const ReDetailMainTableViewCellString;

@protocol ReDetailMainTableViewCellDelegate <NSObject>

- (void)didOutsideWebLinkClicked:(NSURL *)url;
@end
@interface ReDetailMainTableViewCell : UITableViewCell
@property (nonatomic, strong) Recipe *recipe;
@property (nonatomic, weak) id<ReDetailMainTableViewCellDelegate> delegate;
@end
