#import "RePopularCollectionViewCell.h"
#import <RXPromise/RXPromise.h>
#import "ReciptDetailViewModel.h"
#import "UIImageView+DownloadImage.h"

NSString *const RePopularCollectionViewCellString = @"RePopularCollectionViewCell";

@interface RePopularCollectionViewCell()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *backView;

@end

@implementation RePopularCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupContent];
    }
    return self;
}

- (void)setupContent {
    self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"default"]];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.imageView setContentMode:UIViewContentModeScaleToFill];
    [self.contentView addSubview:self.imageView];
    
  
    
    self.backView = [[UIView alloc] initWithFrame:CGRectZero];
    self.backView.translatesAutoresizingMaskIntoConstraints = NO;
    self.backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [self.contentView insertSubview:self.backView aboveSubview:self.imageView];
    
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.titleLabel setFont:[UIFont fontWithName:@"Times New Roman" size:12.0f]];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setText:self.recipe.title];
    self.titleLabel.textAlignment = NSTextAlignmentRight;
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.backView addSubview:self.titleLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    NSMutableArray *constrains = [NSMutableArray array];
    NSDictionary *views = NSDictionaryOfVariableBindings(_imageView,_titleLabel);
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageView]|" options:0 metrics:nil views:views]];
    
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.backView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.backView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.backView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.backView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.0f]];
    
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_titleLabel]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(==5)-[_titleLabel]-(==5)-|" options:0 metrics:nil views:views]];
    
    
    
    [NSLayoutConstraint activateConstraints:constrains];
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}
- (void)setRecipe:(Recipe *)recipe {
    _recipe = recipe;
    [self updateContent];
}

- (void)updateContent {
    self.titleLabel.text = self.recipe.title;
    self.imageView.image = [UIImage imageNamed:@"default"];
    if (self.recipe.thumbnail.length) {
        NSURL *requesturl = [NSURL URLWithString:self.recipe.thumbnail];
        [self.imageView loadImage:requesturl];
    }
}
@end
