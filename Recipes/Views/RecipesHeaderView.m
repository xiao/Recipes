#import "RecipesHeaderView.h"

NSString *const RecipesHeaderViewString = @"RecipesHeaderView";
@interface RecipesHeaderView()
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation RecipesHeaderView


- (instancetype) initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupContentView];
    }
    return self;
}


- (void)setupContentView {
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.titleLabel setFont:[UIFont fontWithName:@"Times New Roman" size:16.0f]];
    [self.titleLabel setTintColor:[UIColor colorWithRed:65.0f/255.0f green:65.0f/255.0f blue:65.0f/255.0f alpha:1.0]];
    [self.contentView addSubview:self.titleLabel];
    self.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSMutableArray *constrains = [NSMutableArray array];
    [constrains addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:20]];
   [constrains addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:14.0f]];
   [constrains addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
    [NSLayoutConstraint activateConstraints:constrains];
}


- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
}

@end
