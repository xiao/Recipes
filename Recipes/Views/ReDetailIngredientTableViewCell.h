#import <UIKit/UIKit.h>
extern NSString *const ReDetailIngredientTableViewCellString;

@interface ReDetailIngredientTableViewCell : UITableViewCell
@property (nonatomic, strong) NSDictionary *ingredient;
@end
