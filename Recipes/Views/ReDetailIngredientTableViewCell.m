#import "ReDetailIngredientTableViewCell.h"
NSString *const ReDetailIngredientTableViewCellString = @"ReDetailIngredientTableViewCell";

@interface ReDetailIngredientTableViewCell()
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@end

@implementation ReDetailIngredientTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.axis = UILayoutConstraintAxisVertical;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void)setIngredient:(NSDictionary *)ingredient {
    _ingredient = ingredient;
    [self updateContent];
}

- (void)updateContent {
    NSArray *keys = self.ingredient.allKeys;
    for (NSString *key in keys) {
        int amount = [[self.ingredient objectForKey:key] intValue];
        [self.stackView addArrangedSubview:[self setupSingleLine:amount withTitle:key]];
    }
}


- (UIStackView *)setupSingleLine:(int)amount withTitle:(NSString *)title {
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.alignment = UIStackViewAlignmentCenter;
    UILabel *amountLabel = [[UILabel alloc] init];
    [[amountLabel.heightAnchor constraintEqualToConstant:30.0f] setActive:YES];
    [[amountLabel.widthAnchor constraintEqualToConstant:60.0f] setActive:YES];

    [amountLabel setFont:[UIFont fontWithName:@"Times New Roman" size:13.0f]];
    [amountLabel setTextColor:[UIColor colorWithRed:65.0f/255.0f green:65.0f/255.0f blue:65.0f/255.0f alpha:1.0]];
    amountLabel.text = [NSString stringWithFormat:@"%d g", amount];
    [stackView addArrangedSubview:amountLabel];

    
    UIStackView *descStack = [[UIStackView alloc] init];
    descStack.alignment = UIStackViewAlignmentLeading;
    descStack.axis = UILayoutConstraintAxisHorizontal;
    UILabel *detailLabel = [[UILabel alloc] init];
    [detailLabel setFont:[UIFont fontWithName:@"Times New Roman" size:13.0f]];
    [[detailLabel.heightAnchor constraintEqualToConstant:30.0f] setActive:YES];
    [detailLabel setTextColor:[UIColor colorWithRed:65.0f/255.0f green:65.0f/255.0f blue:65.0f/255.0f alpha:1.0]];
    detailLabel.text = title;
    [descStack addArrangedSubview:detailLabel];
//    [descStack setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 20)];
//    [descStack setLayoutMarginsRelativeArrangement:YES];
    
    [stackView addArrangedSubview:descStack];
    
    return stackView;
}
@end
