
#import <UIKit/UIKit.h>
static const CGFloat kLineSpacing = 5.f;
static const CGFloat kItemSpacing = 8.f;
static const CGFloat kCellMargins = 5.f;
static const CGFloat kCellHeight  = 100.0f;

@interface HorizontalFlowLayout : UICollectionViewFlowLayout

@end
