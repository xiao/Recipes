#import "RePopularTableViewCell.h"
#import "RePopularCollectionViewCell.h"
#import "HorizontalFlowLayout.h"
#import "Recipe.h"

NSString *const RePopularTableViewCellString = @"RePopularTableViewCell";

@interface RePopularTableViewCell()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, assign) CGFloat rowNumber;
@end

@implementation RePopularTableViewCell
- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupContentView];
        self.rowNumber = 2;
        [[NSNotificationCenter defaultCenter] addObserverForName:@"com.recipe.rotation"  object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            int value = [[note userInfo][@"trait"] intValue];
            if (value == UIUserInterfaceSizeClassCompact) {
                self.rowNumber = 3;
            }else if (value == UIUserInterfaceSizeClassRegular || value == UIUserInterfaceSizeClassUnspecified) {
                self.rowNumber = 2;
            }
            [self.collectionView reloadData];
        }];


    }
    return self;
}

- (void)setRecipes:(NSArray *)recipes {
    _recipes = recipes;
    [self.collectionView reloadData];
}

- (void)setupContentView {
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.frame collectionViewLayout:[[HorizontalFlowLayout alloc] init]];
    self.collectionView.decelerationRate = UIScrollViewDecelerationRateNormal;
    [self.collectionView registerClass:[RePopularCollectionViewCell class] forCellWithReuseIdentifier:RePopularCollectionViewCellString];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    self.collectionView.bounces = false;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.collectionView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSMutableArray *constrains = [NSMutableArray array];
    NSDictionary *views = NSDictionaryOfVariableBindings(_collectionView);
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==20)-[_collectionView]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(==16)-[_collectionView]|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:constrains];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.recipes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RePopularCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RePopularCollectionViewCellString forIndexPath:indexPath];
    cell.recipe = [self.recipes objectAtIndex:indexPath.row];
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 6;
    return cell;
}


- (CGSize)collectionView:(nonnull UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return CGSizeMake((self.frame.size.width - kItemSpacing*(self.rowNumber -1))/self.rowNumber, kCellHeight);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([_delegate respondsToSelector:@selector(didSelectItemInPopularSection:)]) {
        [_delegate didSelectItemInPopularSection:indexPath.row];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
