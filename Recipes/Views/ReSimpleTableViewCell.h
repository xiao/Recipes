#import <UIKit/UIKit.h>
#import "Recipe.h"

extern NSString *const ReSimpleTableViewCellString;

@interface ReSimpleTableViewCell : UITableViewCell
@property (nonatomic, strong) Recipe *recipe;
@end
