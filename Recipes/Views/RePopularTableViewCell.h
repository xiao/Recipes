#import <UIKit/UIKit.h>

extern NSString *const RePopularTableViewCellString;

@protocol RePopularTableViewCellDelegate <NSObject>

- (void)didSelectItemInPopularSection:(NSInteger)index;

@end

@interface RePopularTableViewCell : UITableViewCell
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *recipes;
@property (nonatomic, weak) id<RePopularTableViewCellDelegate> delegate;
@end
