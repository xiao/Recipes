#import "Recipe.h"

@implementation Recipe
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        [self parseObjectFromDictionary:dictionary];
    }
    return self;
}

- (void) parseObjectFromDictionary:(NSDictionary *)dictionary {
    if (dictionary) {
        _title = [[dictionary objectForKey:@"title"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _href = [[dictionary objectForKey:@"href"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *ingredientString = [dictionary objectForKey:@"ingredients"];
        
        if (ingredientString.length > 0) {
            NSArray *components = [ingredientString componentsSeparatedByString:@","];
            NSMutableArray *array = [NSMutableArray array];
            for (int i=0; i<components.count; i++) {
                NSString *item = [components objectAtIndex:i];
                item = [item stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                [array addObject:item];
            }
            _ingredients = [array copy];
        } else {
            _ingredients = [NSArray new];
        }
        
        _thumbnail = [[dictionary objectForKey:@"thumbnail"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}

@end
