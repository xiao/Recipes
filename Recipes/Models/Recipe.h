#import <Foundation/Foundation.h>

@interface Recipe : NSObject
@property(nonatomic, strong, readonly) NSString *title;
@property(nonatomic, strong, readonly) NSString *href;
@property(nonatomic, strong, readonly) NSArray *ingredients;
@property(nonatomic, strong, readonly) NSString *thumbnail;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
