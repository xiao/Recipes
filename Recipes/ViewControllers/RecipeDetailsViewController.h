#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface RecipeDetailsViewController : UIViewController
@property (nonatomic, strong) Recipe *recipe;
@end
