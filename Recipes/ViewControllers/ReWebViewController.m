//
//  ReWebViewController.m
//  Recipes
//
//  Created by jin on 10/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

#import "ReWebViewController.h"
#import "SVProgressHUD.h"

@interface ReWebViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

@end

@implementation ReWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    self.webView.delegate = self;
    [self.closeButton setTintColor:[UIColor blueColor]];
    UIImage *image = [[UIImage imageNamed:@"Cross-White"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeButton setImage:image forState:UIControlStateNormal];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)closeButtonCilicked:(id)sender {
    [self.loadingView stopAnimating];
    self.loadingView.hidden = YES;
    [self.webView stopLoading];
    [self dismissViewControllerAnimated:YES completion:^{
        self.webView = nil;
    }];
}
- (IBAction)back:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

- (IBAction)forward:(id)sender {
    if ([self.webView canGoForward]) {
        [self.webView goForward];
    }
}
- (IBAction)refresh:(id)sender {
    [self.webView reload];
}

#pragma mark -- WebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    self.loadingView.hidden = NO;
    [self.loadingView startAnimating];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.loadingView stopAnimating];
    self.loadingView.hidden = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.loadingView stopAnimating];
    self.loadingView.hidden = YES;
}

@end
