#import "RecipeDetailsViewController.h"
#import "ReDetailMainTableViewCell.h"
#import "ReDetailNutritionTableViewCell.h"
#import "ReDetailIngredientTableViewCell.h"
#import "ReWebViewController.h"


@interface RecipeDetailsViewController ()<UITableViewDelegate, UITableViewDataSource, ReDetailMainTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation RecipeDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
    
    [self.tableView registerNib:[UINib nibWithNibName:ReDetailMainTableViewCellString bundle:[NSBundle mainBundle]] forCellReuseIdentifier:ReDetailMainTableViewCellString];
    [self.tableView registerNib:[UINib nibWithNibName:ReDetailNutritionTableViewCellString bundle:[NSBundle mainBundle]] forCellReuseIdentifier:ReDetailNutritionTableViewCellString];
    [self.tableView registerNib:[UINib nibWithNibName:ReDetailIngredientTableViewCellString bundle:[NSBundle mainBundle]] forCellReuseIdentifier:ReDetailIngredientTableViewCellString];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView setTableFooterView:[UIView new]];
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            return 260.f;
            break;
        case 1:
            return 90.0f;
            break;
        case 2:
            return UITableViewAutomaticDimension;
            break;
        default:
            return UITableViewAutomaticDimension;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return 240.f;
            break;
        case 1:
            return 90.0f;
            break;
        case 2:
            return UITableViewAutomaticDimension;
            break;
        default:
            return UITableViewAutomaticDimension;
            break;
    }
}


#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (indexPath.row) {
        case 0: {
            ReDetailMainTableViewCell *aCell = [tableView dequeueReusableCellWithIdentifier:ReDetailMainTableViewCellString];
            aCell.delegate = self;
            aCell.recipe = self.recipe;
            cell = aCell;
            break;
        }
        case 1:{
            cell = [tableView dequeueReusableCellWithIdentifier:ReDetailNutritionTableViewCellString];
            break;
        }

        case 2:{
            ReDetailIngredientTableViewCell *aCell = [tableView dequeueReusableCellWithIdentifier:ReDetailIngredientTableViewCellString];
            aCell.ingredient = [self parseIngredient:self.recipe.ingredients];
            cell = aCell;
            break;
        }
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSDictionary *)parseIngredient:(NSArray *)ingredients{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *integrent in ingredients) {
        if ([dict objectForKey:integrent]) {
            NSNumber *value = [dict objectForKey:integrent];
            int amount = value.intValue + 1;
            [dict setObject:[NSNumber numberWithInt:amount] forKey:integrent];
        }else {
            [dict setObject:[NSNumber numberWithInt:1] forKey:integrent];
        }
    }
    return dict;
}

#pragma mark -- ReDetailMainTableViewCell Delegate
- (void)didOutsideWebLinkClicked:(NSURL *)url {
    ReWebViewController *webView = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ReWebViewController"];
    webView.url = url;
    [self presentViewController:webView animated:YES completion:nil];
}

@end
