#import <AFNetworking/AFNetworking.h>
#import "ReciptDetailViewModel.h"
#import "AppContext.h"
#import "Recipe.h"

@implementation ReciptDetailViewModel

- (RXPromise *)download {
    RXPromise *promise = [RXPromise new];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:config];
    NSURL *url = [NSURL URLWithString:[AppContext recipeServiceBaseUrl]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {
            [promise rejectWithReason:error];
        } else {
            NSDictionary *dict = (NSDictionary *)responseObject;
            [promise fulfillWithValue:[self handleResponse:dict]];
        }
    }];
    
    [dataTask resume];
    return promise;
}


- (NSArray *)handleResponse:(NSDictionary *)dict {
    NSArray *result = [dict objectForKey:@"results"];
    if (result.count) {
        NSMutableArray *results = [NSMutableArray array];
        for (NSDictionary *objct in result) {
            Recipe *recipt = [[Recipe alloc] initWithDictionary:objct];
            [results addObject:recipt];
        }
        return results;
    }
    
    return [NSArray new];
}
@end
