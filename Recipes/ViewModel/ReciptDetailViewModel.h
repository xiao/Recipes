#import <Foundation/Foundation.h>
#import <RXPromise/RXPromise.h>

@interface ReciptDetailViewModel : NSObject
- (RXPromise *)download;
@end
