//
//  AppDelegate.h
//  Recipes
//
//  Created by jin on 9/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

