#import <Foundation/Foundation.h>

@interface AppContext : NSObject
+ (NSString *)recipeServiceBaseUrl;

@end
