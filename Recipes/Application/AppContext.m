#import "AppContext.h"

@implementation AppContext
static NSDictionary *_environmentConfigs;


+ (NSDictionary *)environmentConfigs {
    if (!_environmentConfigs) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"environment" ofType:@"plist"];
        NSDictionary *configs = [[NSDictionary alloc] initWithContentsOfFile:path];
        _environmentConfigs = configs;
    }
    return _environmentConfigs;
}


+ (NSString *)recipeServiceBaseUrl {
    return [AppContext environmentConfigs][@"RECIPE_URL"];
}

@end
