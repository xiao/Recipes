#import <XCTest/XCTest.h>
#import "Recipe.h"

@interface RecipesTests : XCTestCase
@property(nonatomic, strong) NSDictionary *jsonDict;

@end

@implementation RecipesTests

- (void)setUp {
    [super setUp];
    NSError *error = nil;
    NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"mock" ofType:@"json"];
    
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
    _jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                options:NSJSONReadingMutableContainers
                                                  error:&error];

}

- (void)tearDown {
    [super tearDown];
}

- (void)testParsemockFromDictionary {
    
    NSArray *results = [self.jsonDict objectForKey:@"results"];
    XCTAssertNotNil(results);
    
    Recipe *recipe = [[Recipe alloc] initWithDictionary:results[0]];
    XCTAssertNotNil(recipe);
    
    XCTAssertEqualObjects(recipe.title, @"Savoury Deviled Eggs");
    XCTAssertEqualObjects(recipe.href, @"http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx");
    XCTAssertEqual(recipe.ingredients.count, 1);
    XCTAssertEqualObjects(recipe.thumbnail, @"http://img.recipepuppy.com/627470.jpg");
}

- (void)testIntredeientsArray {
    NSDictionary *dict  = @{@"title":@"   ", @"href":@"    ", @"ingredients":@"eggs,cow,rabbit,chicken"};
    Recipe *recipe = [[Recipe alloc] initWithDictionary:dict];
    XCTAssertEqual(recipe.title.length,0);
    XCTAssertEqual(recipe.href.length,0);
    XCTAssertEqual(recipe.ingredients.count, 4);
    XCTAssertEqualObjects([recipe.ingredients objectAtIndex:3], @"chicken");
    XCTAssertNil(recipe.thumbnail);
}

@end
