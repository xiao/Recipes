# Recipes Project

The explicit requirements are:
 - As a user(of the application) I see the recipes with images in popular section, the recipes is shown in a collection view. image(the whole cell), tile at the right bottom
 - As a user I can see the recipes without images in other section. each item just displayed as a single line with recipe title.
 - As a user I can see 3 coloumns of popular items in landscape mode, 2 columns in portrait
 - As a user I can see the detail info, when I click each item in the first page.
 - As a user I can open the link of recipt in in-app webview.
